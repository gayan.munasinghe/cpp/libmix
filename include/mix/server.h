#ifndef SERVER_H
#define SERVER_H

#include <netinet/in.h>

#include <string>

namespace mix {

namespace sockets {

class Server
{
public:
    void run_server(const std::string& port);
    virtual void on_server_message_in(const std::string& m, const int& fd ) = 0;
    virtual void on_server_message_out(const std::string& message, const int& fd) = 0;
    virtual void on_disconnection(const int& fd) = 0;
    void notify_clients(const std::string& message);
    //virtual void sendMessage() = 0;
private:
    void initialize_client_sockets();
    void update_client_sockets(const int& new_socket);
    void check_socket_io(const fd_set& readfds);
    void accept_connection(const int master_socket, const fd_set& readfds);
    void check_activity(const int& nfds, fd_set& readfds);
    void set_fds(const int& master_socket, int& max_sd, fd_set& readfds);

    int max_clients_{30};
    int client_socket_[30];
};

}

} // namespace mix

#endif // SERVER_H

