#ifndef MIX_SOCKETS_H
#define MIX_SOCKETS_H

#include <netdb.h>
#include <sys/socket.h>

#include <string>
#include <vector>

namespace mix {

namespace sockets {

void *get_in_addr(struct sockaddr *sa);
int get_in_port(struct sockaddr *sa);
void get_dotted_ip(const long& ip, std::string& dotted);
void get_addrinfo(const std::string& host, const std::string& port, struct addrinfo** servinfo, const int ai_family, const int ai_socktype, const int ai_protocol);
int get_socket(addrinfo* servinfo, const bool should_connect = false);
int connect_socket(const std::string& host, const std::string& port);
void read_bytes(int32_t sockfd, void* buffer, int length);
void read_bytes(int32_t sockfd, std::vector<char>& buffer, int length);
void send_bytes(int32_t sockfd, void* data, int length);
void decode_string(int32_t sockfd, std::string& buffer, int size);
void set_socket_options(const int sockfd, const int level, const int optname, const void *optval);
void bind_socket(const int& sockfd, const int port);
void send_message(const int32_t sockfd, const std::string& msg);

template <typename T>
void decode_int(int32_t sockfd, T& buffer) {
    // Decodes 16/32/64 bit integer
    sockets::read_bytes(sockfd, (void*)(&buffer), sizeof(buffer));
    if ((std::is_same<T, uint16_t>::value) || (std::is_same<T, int16_t>::value)) { buffer = ntohs(buffer);}
    else if ((std::is_same<T, uint32_t>::value) || (std::is_same<T, int32_t>::value)) { buffer = ntohl(buffer);}
    else if ((std::is_same<T, uint64_t>::value) || (std::is_same<T, int64_t>::value)) { buffer = be64toh(buffer); }
}

namespace tcp {

int get_socket(const std::string& host, const std::string& port, const bool should_connect = false);
void listen_for_connections(const int& sockfd, const int max_connections);
int accept_connection(const int& master_socket);

} // namespace cp

namespace udp {

void write(const int& sockfd, void* message, const int& length, const int& port, const std::string& remote_ip, const int sin_family = AF_UNSPEC);
void read(const int& sockfd, char (& moldBuffer)[65536]);

namespace mcast {

int connect(const std::string& ip, const int port, const std::string& localInterface);

} // namespace mcast

} // namespace udp

} // namspace sockets

} // namespace mix

#endif // MIX_SOCKETS_H
