#ifndef MIX_EXCEPTION_H
#define MIX_EXCEPTION_H

#include <exception>
#include <string>

namespace mix {

class mix_exception : public std::exception {
public:
    explicit mix_exception(const std::string& what, const std::string& where)
        : what_(what)
        , where_(where) {
    }

    const char* what() const noexcept override
    {
        return what_.c_str();
    }

    std::string where() const
    {
        return where_;
    }

private:
    std::string what_;
    std::string where_;
};

class SocketError : public mix_exception {
public:
    SocketError(const std::string& what, const std::string& where)
        : mix_exception(what, where) {
    }

    SocketError(const std::string& what, const std::string& where, const int sockfd)
        : mix_exception(what, where)
        , sockfd_(sockfd) {
    }

    int sockfd() const {
        return sockfd_;
    }

private:
    int sockfd_;
};

class SocketDisconnectionError : public SocketError {
public:
    SocketDisconnectionError(const std::string& what, const std::string& where, const int sockfd)
        : SocketError(what, where, sockfd) {
    }
};

class FileError : public mix_exception {
public:
    FileError(const std::string& what, const std::string& where, const std::string& file)
        : mix_exception(what, where)
        , file_(file) {
    }

    std::string file() const {
        return file_;
    }

private:
    std::string file_;
};

class FileNotFoundError : public FileError {
public:
    FileNotFoundError(const std::string& what, const std::string& where, const std::string& file)
        : FileError(what, where, file) {
    }
};

class key_error : public mix_exception {
public:
    key_error(const std::string& what, const std::string& where, const std::string& key)
        : mix_exception(what, where)
        , key_(key) {
    }

    std::string key() const {
        return key_;
    }

private:
    std::string key_;
};

class missing_tag : public mix_exception {
public:
    missing_tag(const std::string& what, const std::string& where, const std::string& tag)
        : mix_exception(what, where)
        , tag_(tag) {
    }

    std::string tag() const {
        return tag_;
    }

private:
    std::string tag_;
};

} // end namespace mix

#endif // MIX_EXCEPTION_H

