#include "mix/log.h"

#include <fstream>
#include <iostream>

#include "mix/exception.h"
#include "mix/utility.h"

std::string app_name{"mix"};
mix::log::Logger logger;

namespace mix {

namespace log {

Logger::Logger()
{
    std::string epoch;
    mix::times::get_epoch(epoch, "micro");
    mix::times::get_datetime_from_epoch(epoch, starttime_);
    mix::times::get_datetime_from_epoch(epoch, startdate_, "%Y%m%d");
    read_appname();
    read_log_level();
    read_log_dest();
    read_debug_level();
    modify_log_file_name();
}

int Logger::get_log_level(const std::string& log_level) const
{
    if (log_level == "LOG_EMERG") {
        return 0;
    }
    else if (log_level == "LOG_ALERT") {
        return 1;
    }
    else if (log_level == "LOG_CRIT") {
        return 2;
    }
    else if (log_level == "LOG_ERR") {
        return 3;
    }
    else if (log_level == "LOG_WARNING") {
        return 4;
    }
    else if (log_level == "LOG_NOTICE") {
        return 5;
    }
    else if (log_level == "LOG_INFO") {
        return 6;
    }
    else if (log_level == "LOG_DEBUG") {
        return 7;
    }
    else {
        return -1;
    }
}

std::string Logger::get_log_level(const int log_level) const
{
    switch (log_level) {
    case 0:
        return "LOG_EMERG";
    case 1:
        return "LOG_ALERT";
    case 2:
        return "LOG_CRIT";
    case 3:
        return "LOG_ERR";
    case 4:
        return "LOG_WARNING";
    case 5:
        return "LOG_NOTICE";
    case 6:
        return "LOG_INFO";
    case 7:
        return "LOG_DEBUG";
    default:
        return "?";
    }
}

void Logger::read_appname()
{
    char* envVar;
    if ( (envVar = std::getenv("APPNAME")) ) {
        app_name = envVar;
    }
}

void Logger::read_log_dest()
{
    char* envVar;
    std::string logEnvVar(app_name + "_LOG_DEST");
    mix::strings::upper_case_ascii(logEnvVar);
    if ( (envVar = std::getenv(logEnvVar.c_str())) ) {
        log_dest_ = envVar;
    }
}

void Logger::get_log_dest(std::string& log_dest)
{
    log_dest = log_dest_;
}

void Logger::set_log_dest(const std::string& log_dest)
{
    log_dest_ = log_dest;
}

void Logger::read_log_level()
{
    char* envVar;
    std::string logEnvVar(app_name + "_LOG_LEVEL");
    mix::strings::upper_case_ascii(logEnvVar);
    if ( (envVar = std::getenv(logEnvVar.c_str())) ) {
        log_level_ = get_log_level(std::string(envVar));
    }
}

void Logger::set_log_level(int log_level)
{
    log_level_ = log_level;
}

void Logger::read_debug_level()
{
    char* envVar;
    std::string logEnvVar(app_name + "_DEBUG_LEVEL");
    mix::strings::upper_case_ascii(logEnvVar);
    if ( (envVar = std::getenv(logEnvVar.c_str())) ) {
        try {
            debug_level_ = std::stoi(std::string(envVar));
        }
        catch (const std::invalid_argument& e) {
            throw mix::mix_exception(std::string("std::invalid_argument ") + e.what(), mix::strings::where(__LINE__, __FILE__, __PRETTY_FUNCTION__));
        }
    }
}

void Logger::set_debug_level(int debug_level)
{
    debug_level_ = debug_level;
}

void Logger::reset_app_name(const std::string& app) const
{
    char* envVar;
    if (!(envVar = std::getenv("APPNAME"))) {
        app_name = app;
    }
}

void Logger::set_log_file_name(const std::string& log_file)
{
    log_file_ = log_file;
    modify_log_file_name();
}

void Logger::set_log_file_name_append(const std::string& append)
{
    log_file_name_append_ = append;
    modify_log_file_name();
}

void Logger::set_timestamp_format(const std::string& timestamp_format) {
    timestamp_format_ = timestamp_format;
}

void Logger::modify_log_file_name()
{
    const auto& pos = log_file_.find(".");
    const std::string& fileNamePart = log_file_.substr(0, pos);
    if (log_file_name_append_.compare("datetime") == 0) { log_file_ = fileNamePart + "." + starttime_ + ".log"; }
    else { log_file_ = fileNamePart + "." + startdate_ + ".log"; }
}

const std::string Logger::get_time_strings() const
{
    std::string epoch;
    std::string dateTimeT;
    std::string decimalsT;
    mix::times::get_epoch(epoch, "micro", dateTimeT, decimalsT);
    if (timestamp_format_ == "epoch") {
        return dateTimeT + "." + decimalsT;
    }
    else {
        return mix::times::get_datetime_from_epoch(dateTimeT, "%FT%T." + decimalsT + "%z[%Z]");
    }
}

void Logger::add_log(int log_level, const std::string& msg)
{
    add_log(log_level, log_dest_, msg);
}

void Logger::add_log(int log_level, const std::string& log_dest, const std::string& msg, const std::string& debug_level)
{
    read_log_level();
    if (log_level <= log_level_) {
        if (log_dest.compare("null") == 0) { ;; }
        else if (log_dest == "console") { add_console_log(log_level, msg, debug_level); }
        else if (log_dest == "syslog") { add_sys_log(log_level, msg); }
        else if (log_dest == "file") { add_file_log(log_level, msg, debug_level); }
    }
}

void Logger::add_log(const int log_level, const std::string& log_dest, const std::string& msg, const int debug_level)
{
    if (debug_level_ > 0) {
        if (debug_level_ >= debug_level) {
            add_log(log_level, log_dest, msg, std::to_string(debug_level));
        }
    }
}

void Logger::add_log(const int log_level, const std::string& msg, const int debug_level)
{
    add_log(log_level, log_dest_, msg, debug_level);
}

void Logger::add_console_log(int log_level, const std::string& msg, const std::string& debug_level) const
{
    write_log(log_level, msg, std::cout, debug_level);
}

void Logger::add_file_log(int log_level, const std::string& msg, const std::string& debug_level) const
{
    std::ofstream flogger(log_file_, std::ios_base::out | std::ios_base::app );
    write_log(log_level, msg, flogger, debug_level);
}

void Logger::write_log(int log_level, const std::string& msg, std::ostream& stream, const std::string& debug_level) const
{
    const std::string& timestamp = get_time_strings();
    auto debug_level_str = debug_level.empty() ? "" : " " + debug_level;
    stream << timestamp << " " << app_name << " " << get_log_level(log_level) << debug_level_str << ": " << msg;
}

void Logger::add_sys_log(int log_level, const std::string& msg) const
{
     openlog(app_name.c_str(), LOG_PID|LOG_CONS, LOG_USER);
     syslog(log_level, msg.c_str());
     closelog();
}

}

}
