#include "mix/utility.h"

#include <arpa/inet.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/time.h>

#include <cstring>
#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>

#include "rapidjson/pointer.h"
#include <rapidjson/filereadstream.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>
#include "mix/exception.h"

using namespace rapidjson;

namespace mix {

int send_mail(const std::string& to,
             const std::string& from,
             const std::string& subject,
             const std::string& message,
             const std::string& cc,
             const std::string& bcc)
{
    int retval = -1;
    FILE *mailpipe = popen("/usr/lib/sendmail -t", "w");
    if (mailpipe != NULL) {
        fprintf(mailpipe, "To: %s\n", to.c_str());
        fprintf(mailpipe, "From: %s\n", from.c_str());
        fprintf(mailpipe, "CC: %s\n", cc.c_str());
        fprintf(mailpipe, "BCC: %s\n", bcc.c_str());
        fprintf(mailpipe, "Subject: %s\n\n", subject.c_str());
        fwrite(message.c_str(), 1, strlen(message.c_str()), mailpipe);
        fwrite(".\n", 1, 2, mailpipe);
        pclose(mailpipe);
        retval = 0;
     }
     else {
         perror("Failed to invoke sendmail");
     }
     return retval;
}

namespace json {

bool key_exists(const Document& d, const std::string& msg)
{
    for (auto& it : d.GetObject()) {
        if (msg == it.name.GetString()) {
            return true;
        }
    }
    return false;
}

std::string get_string(Document& d, const std::string& s)
{
    GenericPointer<Value> p(s.c_str());
    Value* value = GetValueByPointer(d, p);
    //Some searches were returning null values for keys that were not supposed to
    //be existing. My guess is that a possible corruption might be occuring due to
    //copying json objects. Had to add a IsNul() check, but this code might be
    //buggy. Need to come back to this later.
    if ((value == nullptr) || (value->IsNull())) {
        return "";
    }
    else {
        return value->GetString();
    }
}

int get_positive_int(Document& d, const std::string& s)
{
    GenericPointer<Value> p(s.c_str());
    Value* value = GetValueByPointer(d, p);
    if ((value == nullptr) || (value->IsNull())) {
        return -1;
    }
    else {
        return value->GetInt();
    }
}

//Value::ConstMemberIterator get_object(Document& d, const std::string& s)
//{
//    if (d.HasMember(s.c_str())) {
//        return d[s].GetObject();
//    }
//    GenericPointer<Value> p(s.c_str());
//    Value* value = GetValueByPointer(d, p);
//    if (value == nullptr) {
//        throw mix::key_error("\"" + s + "\" Not found", mix::strings::where(__LINE__, __FILE__, __PRETTY_FUNCTION__), s);
//    }
//    else {
//        return value->GetObject();
//    }
//}
//
void parse_string(const std::string& s, Document& d)
{
    d.Parse(s.c_str());
}

void read_config(const std::string& config_file, Document& d)
{
    FILE* fp = fopen(config_file.c_str(), "r"); // non-Windows use "r"
    char readBuffer[65536];
    FileReadStream is(fp, readBuffer, sizeof(readBuffer));
    d.ParseStream(is);
    fclose(fp);
}

void copy_document(const Document& d, Document& n)
{
    n.CopyFrom(d, n.GetAllocator());
}

void print(const Document& d)
{
    StringBuffer buffer;
    Writer<StringBuffer> writer(buffer);
    d.Accept(writer);
    std::cout << buffer.GetString() << std::endl;
}

} // namespace json

namespace strings {

unsigned char hexval(unsigned char c)
{
    if ('0' <= c && c <= '9')
        return c - '0';
    else if ('a' <= c && c <= 'f')
        return c - 'a' + 10;
    else if ('A' <= c && c <= 'F')
        return c - 'A' + 10;
    else abort();
}

void string_to_hex(const std::string& input, std::string& output)
{
    static const char* const lut = "0123456789ABCDEF";
    size_t len = input.length();
    output.reserve(2 * len);
    for (size_t i = 0; i < len; ++i) {
        const unsigned char c = input[i];
        output.push_back(lut[c >> 4]);
        output.push_back(lut[c & 15]);
    }
}

void hex2ascii(const std::string& in, std::string& out)
{
    out.clear();
    out.reserve(in.length() / 2);
    for (std::string::const_iterator p = in.begin(); p != in.end(); ++p)
    {
       unsigned char c = mix::strings::hexval(*p);
       ++p;
       if (p == in.end()) break; // incomplete last digit - should report error
       c = (c << 4) + hexval(*p); // + takes precedence over <<
       out.push_back(c);
    }
}

void split(const std::string &s, char delim, std::vector<std::string> &elems)
{
    std::stringstream ss(s);
    std::string item;
    while (getline(ss, item, delim)) {
        elems.push_back(item);
    }
}

void right_pad_with_spaces(std::string& original, int numberOfCharactors)
{
    std::ostringstream oss;
    oss << original << std::setw(numberOfCharactors - original.size()) << "";
    original = oss.str();
}

void left_pad_with_spaces(std::string& original, int numberOfCharactors)
{
    std::ostringstream oss;
    oss << std::setw(numberOfCharactors) << original;
    original = oss.str();
}

void left_pad(std::string& s, int padded_len, const char& pad_char)
{
    s.insert(s.begin(), padded_len - s.size(), pad_char);
}

void char_to_string(char c, std::string& s)
{
    std::stringstream ss;
    ss << c;
    ss >> s;
}

void replace_all(std::string& source, const std::string& from, const std::string& to)
{
    std::string newString;
    newString.reserve(source.length());

    std::string::size_type lastPos = 0;
    std::string::size_type findPos;

    while(std::string::npos != (findPos = source.find(from, lastPos))) {
        newString.append(source, lastPos, findPos - lastPos);
        newString += to;
        lastPos = findPos + from.length();
    }

    newString += source.substr(lastPos);
    source.swap(newString);
}

// void readConfig(const std::string& fileName, json& config)
// {
//     if (!mix::files::file_exists(fileName)) {
//         throw mix::FileNotFoundError("File not found", mix::strings::where(__LINE__, __FILE__, __PRETTY_FUNCTION__), fileName);
//     }
//     std::ifstream instream(fileName);
//     instream >> config;
// }

int string_byte_to_int(const std::string& s)
{
    uint16_t* data = (uint16_t*) (&s[0]);
    uint16_t len = ntohs(*data);
    int length = len;
    return length;
}

std::string where(const int line, const std::string& file, const std::string& function) {
    return file + ":" + std::to_string(line) + ":" + function;
}

void format_time(const std::string& dateTimeT, std::string& formatedT, const std::string& decimalsT = "" )
{
    formatedT = dateTimeT.substr(0,4) + "-"
        + dateTimeT.substr(4,2) + "-"
        + dateTimeT.substr(6,2) + "T"
        + dateTimeT.substr(8,2) + ":"
        + dateTimeT.substr(10,2) + ":"
        + dateTimeT.substr(12,2);
    if ( decimalsT != "" ) {
        formatedT = formatedT + "." + decimalsT;
    }
}

void upper_case_ascii(std::string& s)
{
    for(int i=0;s[i]!=0;i++) {
        if(s[i]<='z' && s[i]>='a') {
            s[i]-=32;
        }
    }
}

void strip_non_alphanumeric(std::string& s)
{
    s.erase(remove_if(s.begin(),s.end(), std::not1(std::ptr_fun( (int(*)(int))std::isalnum ))), s.end());
}

void erase_chars(std::string& s, const char c)
{
    s.erase (std::remove (s.begin(), s.end(), c), s.end());
}

} // namespace strings

namespace times {

std::string get_gmt_time(const std::string& fmt) {
  char buffer[30];
  struct timeval tv;
  time_t curtime;
  gettimeofday(&tv, NULL);
  curtime=tv.tv_sec;
  strftime(buffer, 30, fmt.c_str(), gmtime(&curtime));
  return std::string(buffer) + "." + std::to_string(tv.tv_usec);
}

void get_time(char* date, std::size_t size, const std::string& format)
{
    time_t now = time(NULL);
    strftime(date, size, format.c_str(), localtime(&now));
}

void get_time(std::string& date, std::size_t size, const std::string& format)
{
    std::vector<char> v_date(date.begin(), date.end());
    v_date.push_back('\0');
    time_t now = time(NULL);
    std::strftime(&v_date[0], size, format.c_str(), localtime(&now));
    date = std::string(v_date.data());
}

void epoch_micro_time(std::string& microTime)
{
    auto ms = std::chrono::duration_cast< std::chrono::microseconds >(
        std::chrono::system_clock::now().time_since_epoch() // 1st January 1970 00:00:00
    );

    std::string tstring;
    tstring = std::to_string(ms.count());
    microTime = tstring.substr(0,10) + "." + tstring.substr(10,15);
}

void get_epoch(std::string& epoch)
{
     auto ms = std::chrono::duration_cast< std::chrono::seconds >(
         std::chrono::system_clock::now().time_since_epoch()
     );
     epoch = std::to_string(ms.count());
}

std::string get_epoch()
{
    auto ms = std::chrono::duration_cast< std::chrono::seconds >(
        std::chrono::system_clock::now().time_since_epoch()
    );
    return std::to_string(ms.count());
}

void get_epoch(std::string& epoch, const std::string& details)
{
    if ( details == "mili" ) {
        get_epoch_mili(epoch);
    } else if ( details == "micro" ) {
        get_epoch_micro(epoch);
    }
}

void get_epoch(std::string& epoch, const std::string& details, std::string& dateTimeT, std::string& decimalsT)
{
    if ( details == "mili" ) {
        get_epoch_mili(epoch);
        mix::times::get_decimal_seconds_from_epoch(epoch, decimalsT, "milli");
    } else if ( details == "micro" ) {
        get_epoch_micro(epoch);
        mix::times::get_decimal_seconds_from_epoch(epoch, decimalsT, "micro");
    }
    get_datetime_from_epoch_with_decimal_seconds(epoch, dateTimeT);
}

void get_epoch_mili(std::string& epoch)
{
     auto ms = std::chrono::duration_cast< std::chrono::milliseconds >(
         std::chrono::system_clock::now().time_since_epoch()
     );
     epoch = std::to_string(ms.count());
}

void get_epoch_micro(std::string& epoch)
{
     auto ms = std::chrono::duration_cast< std::chrono::microseconds >(
         std::chrono::system_clock::now().time_since_epoch()
     );

     epoch = std::to_string(ms.count());
}

void get_datetime_from_epoch(const std::string& epoch, std::string& dateTimeT)
{
     char buffer[40];
     time_t t = atoi(epoch.substr(0,10).c_str());
     struct tm* timeinfo = localtime ( &t );
     strftime(buffer, 40, "%Y%m%d%H%M%S", timeinfo);
     dateTimeT = buffer;
}

std::string get_datetime_from_epoch(const int epoch, const std::string& format)
{
    std::string epochStr = std::to_string(epoch);
    return get_datetime_from_epoch(epochStr, format);
}

void get_datetime_from_epoch(const int epoch, std::string& dateTimeT, const std::string& format)
{
    std::string epochStr;
    epochStr = std::to_string(epoch);
    get_datetime_from_epoch(epochStr, dateTimeT, format);
}

std::string get_datetime_from_epoch(const std::string& epoch, const std::string& format)
{
     char buffer[40];
     time_t t = atoi(epoch.substr(0,10).c_str());
     struct tm* timeinfo = localtime ( &t );
     strftime(buffer, 40, format.c_str(), timeinfo);
     return buffer;
}

void get_datetime_from_epoch(const std::string& epoch, std::string& dateTimeT, const std::string& format)
{
     char buffer[40];
     time_t t = atoi(epoch.substr(0,10).c_str());
     struct tm* timeinfo = localtime ( &t );
     strftime(buffer, 40, format.c_str(), timeinfo);
     dateTimeT = buffer;
}

void get_decimal_seconds_from_epoch(const std::string& epoch, std::string& decimalT, const std::string& details)
{
    if ( details == "mili" ) {
        decimalT = epoch.substr(10,3);
    } else if ( details == "micro" ) {
        decimalT = epoch.substr(10,6);
    }
}

void get_datetime_from_epoch_with_decimal_seconds(const std::string& epoch, std::string& dateTimeT) {
    dateTimeT = epoch.substr(0,10);
}

} // namespace times

namespace files {

bool file_exists(const std::string& name)
{
  struct stat buffer;
  return (stat (name.c_str(), &buffer) == 0);
}

void IniParser::read(const std::string& fileName)
{
    std::ifstream configFile;
    std::string section, line, sectionStart, key, value;

    configFile.open(fileName.c_str());
    if (configFile.is_open()) {
        while ( std::getline (configFile, line) ) {
            if ( line[0]=='#' ) {continue;}
            std::istringstream is_line(line);
            if ( line[0]=='[' ) {
                if ( getline(is_line, sectionStart, '[') ) {
                    if ( getline(is_line, section, ']') ) {}
                }
            }
            if ( getline(is_line, key, '=') ) {
                if ( getline(is_line, value) ) {
                    config_[section][key]=value;
                }
            }
        }
        configFile.close();
    }
    else std::cout << "Unable to open " << fileName;
}

void IniParser::get(const std::string& section, const std::string& key, std::string& value)
{
    value = config_[section][key];
}

const std::string& IniParser::get(const std::string& section, const std::string& key) const {
    auto it = config_.find(section);
    if (it != config_.end()) {
        auto inIt = it->second.find(key);
        if (inIt != it->second.end()) {
            return inIt->second;
        }
    }
    return value_;
}

std::string& IniParser::get(const std::string& section, const std::string& key) {
    auto it = config_.find(section);
    if (it != config_.end()) {
        auto inIt = it->second.find(key);
        if (inIt != it->second.end()) {
            return inIt->second;
        }
    }
    return value_;
}

} // namespace files

} // namespace mix

