#ifndef MIX_GLOBALS_H
#define MIX_GLOBALS_H

#include <string>

#include "mix/log.h"

extern std::string app_name;
extern mix::log::Logger logger;
extern std::string mix_config_file;

#endif // MIX_GLOBALS_H
