#ifndef MIX_LOG_H
#define MIX_LOG_H

#include <syslog.h>

#include <map>
#include <string>

namespace mix {

namespace log {

class Logger {

public:
    Logger();
    void add_log(const int log_level, const std::string& log_dest, const std::string& msg, const int debug_level);
    void add_log(const int log_level, const std::string& msg, const int debug_level);
    void add_log(int log_level, const std::string& log_dest, const std::string& msg, const std::string& debug_level="");
    void add_log(int log_level, const std::string& msg);
    void modify_log_file_name();
    void reset_app_name(const std::string& app_name) const;
    void set_debug_level(int debug_level);
    void set_log_dest(const std::string& log_dest);
    void set_log_file_name(const std::string& log_file);
    void set_log_file_name_append(const std::string& append);
    void set_log_level(int log_level);
    void set_timestamp_format(const std::string& timestamp_format);

private:
    const std::string get_time_strings() const;
    int get_log_level(const std::string& log_level) const;
    std::string get_log_level(const int log_level) const;
    void add_console_log(int log_level, const std::string& msg, const std::string& debug_level) const;
    void add_file_log(int log_level, const std::string& msg, const std::string& debug_level) const;
    void add_sys_log(int log_level, const std::string& msg) const;
    void get_log_dest(std::string& log_dest);
    void read_appname();
    void read_debug_level();
    void read_log_dest();
    void read_log_level();
    void write_log(int log_level, const std::string& msg, std::ostream& stream, const std::string& debug_level) const;

    int debug_level_;
    int log_level_{LOG_INFO};
    std::string log_dest_{"null"};
    std::string log_file_name_append_{"date"};
    std::string log_file_{"log/logger.log"};
    std::string startdate_;
    std::string starttime_;
    std::string timestamp_format_{"epoch"};
};

} // namespace log

} // namespace mix

#endif // MIX_LOG_H

