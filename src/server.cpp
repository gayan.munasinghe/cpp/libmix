#include "mix/server.h"

#include <stdio.h>
#include <string.h>   //strlen
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>   //close
#include <arpa/inet.h>    //close
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/time.h> //FD_SET, FD_ISSET, FD_ZERO macros

#include "mix/exception.h"
#include "mix/sockets.h"
#include "mix/log.h"
#include "mix/globals.h"
#include "mix/utility.h"

#define TRUE   1
#define FALSE  0
#define PORT 2018

namespace mix {

namespace sockets {

void Server::notify_clients(const std::string& message)
{
    for (int i = 0; i < 30; i++) {
        int sd = client_socket_[i];
        if (sd > 0) {
            mix::sockets::send_bytes(sd, (void*)(message.c_str()), message.size());
        }
    }
}

void Server::initialize_client_sockets()
{
    for (int i = 0; i < max_clients_; i++)
    {
        client_socket_[i] = 0;
    }
}

void Server::update_client_sockets(const int& new_socket)
{
    for (int i = 0; i < max_clients_; i++) {
        if(client_socket_[i] == 0) {
            client_socket_[i] = new_socket;
            logger.add_log(LOG_DEBUG, "Adding to list of sockets as " + std::to_string(i) + "\n");
            break;
        }
    }
}

void Server::run_server(const std::string& port)
{
    int opt = TRUE;
    int master_socket, max_sd;

    //set of socket descriptors
    fd_set readfds;

    initialize_client_sockets();

    //create a master socket
    master_socket = mix::sockets::tcp::get_socket("127.0.0.1", port);
    mix::sockets::set_socket_options(master_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt);
    mix::sockets::bind_socket(master_socket, PORT);
    logger.add_log(LOG_DEBUG, std::string("Listener on port ") + std::to_string(PORT) + "\n");

    //try to specify maximum of 3 pending connections for the master socket
    mix::sockets::tcp::listen_for_connections(master_socket, 3);

    while(TRUE)
    {
        set_fds(master_socket, max_sd, readfds);
        check_activity(max_sd + 1, readfds);
        accept_connection(master_socket, readfds);
        check_socket_io(readfds);
    }

}

void Server::set_fds(const int& master_socket, int& max_sd, fd_set& readfds)
{
    int sd;
    //clear the socket set
    FD_ZERO(&readfds);

    //add master socket to set
    FD_SET(master_socket, &readfds);
    max_sd = master_socket;

    //add child sockets to set
    for (int i = 0 ; i < max_clients_ ; i++) {
        //socket descriptor
        sd = client_socket_[i];
        //if valid socket descriptor then add to read list
        if(sd > 0) {
            FD_SET( sd , &readfds);
        }
        //highest file descriptor number, need it for the select function
        if(sd > max_sd) {
            max_sd = sd;
        }
    }
}

void Server::check_activity(const int& nfds, fd_set& readfds)
{
    int activity = select(nfds, &readfds ,NULL ,NULL ,NULL);
    if ((activity < 0) && (errno!=EINTR)) {
        logger.add_log(LOG_DEBUG, "Select error\n");
        throw mix::mix_exception("Select error", mix::strings::where(__LINE__, __FILE__, __PRETTY_FUNCTION__));
    }
}

void Server::accept_connection(const int master_socket, const fd_set& readfds)
{
    if (FD_ISSET(master_socket, &readfds))
    {
        int new_socket = mix::sockets::tcp::accept_connection(master_socket);
        update_client_sockets(new_socket);
    }

}

void Server::check_socket_io(const fd_set& readfds)
{
    for (int i = 0; i < max_clients_; i++) {
        int sd = client_socket_[i];
        if (FD_ISSET( sd , &readfds)) {
            try {
                std::string data;
                mix::sockets::decode_string(sd, data, 1024);
                on_server_message_in(data, sd);
            }
            catch (const mix::SocketDisconnectionError& e) {
                close( sd );
                on_disconnection(sd);
                client_socket_[i] = 0;
            }
        }
    }
}

}

}
