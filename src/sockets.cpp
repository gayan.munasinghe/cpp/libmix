#include "mix/sockets.h"

#include <arpa/inet.h>
#include <sys/types.h>
#include <unistd.h>

#include <cstring>

#include "mix/exception.h"
#include "mix/globals.h"
#include "mix/log.h"
#include "mix/utility.h"

#include <iostream>

namespace mix {
namespace sockets {

void* get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

int get_in_port(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return (((struct sockaddr_in*)sa)->sin_port);
    }

    return (((struct sockaddr_in6*)sa)->sin6_port);

}

void get_dotted_ip(const long& ip, std::string& dotted) {
    long numeric_ip;
    numeric_ip = htonl(ip);
    struct in_addr addr;
    addr.s_addr = numeric_ip;
    char *dot_ip = inet_ntoa(addr);
    dotted = dot_ip;
}

void get_addrinfo(const std::string& host, const std::string& port, struct addrinfo** servinfo, const int ai_family, const int ai_socktype, const int ai_protocol)
{
    int status;
    struct addrinfo hints;
    memset(&hints, 0, sizeof hints);
    hints.ai_family = ai_family;
    hints.ai_socktype = ai_socktype;
    hints.ai_protocol = ai_protocol;
    if ((status = getaddrinfo(host.c_str(), port.c_str(), &hints, servinfo)) != 0) {
        logger.add_log(LOG_ERR, std::string("getaddrinfo error: ") + gai_strerror(status) + "\n");
        throw mix::SocketError(std::string("getaddrinfo error: ") + gai_strerror(status), mix::strings::where(__LINE__, __FILE__, __PRETTY_FUNCTION__));
    }
}

int get_socket(addrinfo* servinfo, const bool should_connect)
{
    int sockfd;
    struct addrinfo *p;
    for(p = servinfo; p != NULL; p = p->ai_next) {
        if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
            logger.add_log(LOG_ERR, "Socket error\n");
            continue;
        }
        if (should_connect) {
            if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
                logger.add_log(LOG_ERR, "Client error\n");
                close(sockfd);
                continue;
            }
        }
        break;
    }
    if (p == NULL) {
        logger.add_log(LOG_ERR, "Socket creation failed\n");
        throw mix::SocketError("Socket creation failed", mix::strings::where(__LINE__, __FILE__, __PRETTY_FUNCTION__));
    } else {
        logger.add_log(LOG_DEBUG, "Socket created succesfully\n");
    }
    return sockfd;
}

int connect_socket(const std::string& host, const std::string& port)
{
    struct addrinfo *servinfo;
    mix::sockets::get_addrinfo(host, port, &servinfo, AF_UNSPEC, SOCK_STREAM, 0);
    logger.add_log(LOG_DEBUG, "Connecting to " + host + " " + port + "\n");
    int32_t sockfd = mix::sockets::get_socket(servinfo, true);
    logger.add_log(LOG_INFO, "Succesfully connected\n");
    freeaddrinfo(servinfo);
    return sockfd;
}

void read_bytes(int32_t sockfd, void* buffer, int length)
{
    int numbytes;
    struct sockaddr_in address;
    int addrlen = sizeof(address);
    if ((numbytes = recv(sockfd, buffer, length, 0)) == -1) {
        logger.add_log(LOG_ERR, "Error reading data\n");
        throw mix::SocketError("Error reading data", mix::strings::where(__LINE__, __FILE__, __PRETTY_FUNCTION__));
    }
    if (numbytes == 0) {
        getpeername(sockfd , (struct sockaddr*)&address , (socklen_t*)&addrlen);
        std::string disconnection_str = std::string(inet_ntoa(address.sin_addr)) + ":" + std::to_string(ntohs(address.sin_port)) + " disconnected";
        logger.add_log(LOG_ERR, disconnection_str + "\n");
        throw mix::SocketDisconnectionError(disconnection_str, mix::strings::where(__LINE__, __FILE__, __PRETTY_FUNCTION__), sockfd);
    }
}

void read_bytes(int32_t sockfd, std::vector<char>& buffer, int length)
{
    int numbytes;
    struct sockaddr_in address;
    int addrlen = sizeof(address);
    if ((numbytes = recv(sockfd, buffer.data(), length, 0)) == -1) {
        logger.add_log(LOG_ERR, "Error reading data\n");
        throw mix::SocketError("Error reading data", mix::strings::where(__LINE__, __FILE__, __PRETTY_FUNCTION__));
    }
    if (numbytes == 0) {
        getpeername(sockfd , (struct sockaddr*)&address , (socklen_t*)&addrlen);
        std::string disconnection_str = std::string(inet_ntoa(address.sin_addr)) + ":" + std::to_string(ntohs(address.sin_port)) + " disconnected";
        logger.add_log(LOG_ERR, disconnection_str + "\n");
        throw mix::SocketDisconnectionError(disconnection_str, mix::strings::where(__LINE__, __FILE__, __PRETTY_FUNCTION__), sockfd);
    }
}

void send_bytes(int32_t sockfd, void* data, int length)
{
    int bytecount;
    if((bytecount = send(sockfd, (const void*)(data), length, 0))== -1) {
        logger.add_log(LOG_ERR, "Error sending data\n");
        throw mix::SocketError("Error sending data", mix::strings::where(__LINE__, __FILE__, __PRETTY_FUNCTION__));
    }
}

void send_message(const int32_t sockfd, const std::string& msg)
{
    send_bytes(sockfd, (void*)(msg.c_str()), sizeof(msg));
}

void decode_string(int32_t sockfd, std::string& buffer, int size)
{
    buffer.resize(size);
    sockets::read_bytes(sockfd, (void*)(&buffer[0]), size);
}

void set_socket_options(const int sockfd, const int level, const int optname, const void *optval)
{
    if(setsockopt(sockfd, level, optname, optval, sizeof(optval)) < 0) {
        logger.add_log(LOG_ERR, "Error occured while setting socket options\n");
        close(sockfd);
        throw mix::SocketError("Error occured while setting socket options", mix::strings::where(__LINE__, __FILE__, __PRETTY_FUNCTION__));
    } else {
        logger.add_log(LOG_DEBUG, "Setting socket options succeeded.\n");
    }
}

void bind_socket(const int& sockfd, const int port)
{
    struct sockaddr_in address;
    memset((char *) &address, 0, sizeof(address));
    address.sin_family = AF_INET;
    address.sin_port = htons(port);
    address.sin_addr.s_addr = INADDR_ANY;
    if(bind(sockfd, (struct sockaddr*)&address, sizeof(address))) {
        logger.add_log(LOG_ERR, "Binding socket error\n");
        close(sockfd);
        throw mix::SocketError("Binding socket error", mix::strings::where(__LINE__, __FILE__, __PRETTY_FUNCTION__));
    } else {
        logger.add_log(LOG_DEBUG, "Binding socket...OK.\n");
    }
}

namespace tcp {

int get_socket(const std::string& host, const std::string& port, const bool should_connect)
{
    struct addrinfo *servinfo;
    mix::sockets::get_addrinfo(host, port, &servinfo, AF_UNSPEC, SOCK_STREAM, 0);
    if (should_connect) {
        logger.add_log(LOG_DEBUG, "Connecting to " + host + " " + port + "\n");
    }
    int32_t sockfd = mix::sockets::get_socket(servinfo, should_connect);
    if (should_connect) {
        logger.add_log(LOG_DEBUG, "Succesfully connected\n");
    }
    freeaddrinfo(servinfo);
    return sockfd;
}

void listen_for_connections(const int& sockfd, const int max_connections)
{
    if (listen(sockfd, max_connections) < 0) {
        logger.add_log(LOG_ERR, "Listening failed\n");
        close(sockfd);
        throw mix::SocketError("Listening failed", mix::strings::where(__LINE__, __FILE__, __PRETTY_FUNCTION__));
    }
    logger.add_log(LOG_DEBUG, "Waiting for connections\n");
}

int accept_connection(const int& master_socket)
{
    int new_socket;
    struct sockaddr_in address;
    int addrlen = sizeof(address);
    if ((new_socket = accept(master_socket, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        logger.add_log(LOG_ERR, "Error accepting connection\n");
        throw mix::SocketError("Error accepting connection", mix::strings::where(__LINE__, __FILE__, __PRETTY_FUNCTION__));
    }
    logger.add_log(LOG_DEBUG, "New connection, socket fd: " + std::to_string(new_socket) + " ip: " +
                              inet_ntoa(address.sin_addr) + " port: " + std::to_string(ntohs(address.sin_port)) + "\n");
    return new_socket;
}

} // namespace tcp

namespace udp {

void write(const int& sockfd, void* message, const int& length, const int& port, const std::string& remote_ip, const int sin_family)
{
    struct sockaddr_in remote;
    remote.sin_family = sin_family;
    remote.sin_port = htons(port);
    remote.sin_addr.s_addr = inet_addr(remote_ip.c_str());
    socklen_t addrSize;
    addrSize = sizeof(remote);
    memset(remote.sin_zero, '\0', sizeof(remote.sin_zero));
    sendto(sockfd, (const void*)message, length, 0, (struct sockaddr *)&remote, addrSize);
}

void read(const int& sockfd, char (& moldBuffer)[65536])
{
    int numbytes;
    if ((numbytes = recv(sockfd, moldBuffer, sizeof(moldBuffer), 0)) == -1) {
        logger.add_log(LOG_ERR, "Reading datagram message error\n");
        close(sockfd);
    }
}

namespace mcast {

int connect(const std::string& ip, const int port, const std::string& localInterface)
{
    int reuse;
    struct ip_mreq group;
    struct addrinfo *servinfo;
    group.imr_multiaddr.s_addr = inet_addr(ip.c_str());
    group.imr_interface.s_addr = inet_addr(localInterface.c_str());

    mix::sockets::get_addrinfo(ip, std::to_string(port), &servinfo, AF_UNSPEC, SOCK_DGRAM, 0);
    int32_t sockfd = mix::sockets::get_socket(servinfo);
    mix::sockets::set_socket_options(sockfd, SOL_SOCKET, SO_REUSEADDR, (char *)&reuse);
    mix::sockets::bind_socket(sockfd, port);
    mix::sockets::set_socket_options(sockfd, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *)&group);
    return sockfd;
}

} // namespace mcast

} // namespace udp

} // namespace sockets

} // namespace mix

