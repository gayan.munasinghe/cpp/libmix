#ifndef MIX_UTILITY_H
#define MIX_UTILITY_H

#include <algorithm>
#include <map>
#include <string>
#include <vector>

//#include "mix/json.hpp"

#include "rapidjson/document.h"

using namespace rapidjson;

//using json = nlohmann::json;

namespace mix {

template <typename T, typename S>
bool has_key(const std::map<T,S>& m, const T& key) {
    return (m.count(key)) > 0;
}

template <typename T>
bool has_element(const std::vector<T>& v, const T& element) {
    return std::find(v.begin(), v.end(), element) != v.end();
}

int send_mail(const std::string& to,
             const std::string& from,
             const std::string& subject,
             const std::string& message,
             const std::string& cc = "",
             const std::string& bcc = "");

namespace json {

bool key_exists(const Document& d, const std::string& msg);
std::string get_string(Document& d, const std::string& s);
void read_config(const std::string& config_file, Document& d);
void copy_document(const Document& d, Document& n);
void parse_string(const std::string& s, Document& d);
void print(const Document& d);
int get_positive_int(Document& d, const std::string& s);

}

namespace strings {

void split(const std::string &s, char delim, std::vector<std::string> &elems);
void right_pad_with_spaces(std::string& original, int numberOfCharactors);
void left_pad_with_spaces(std::string& original, int numberOfCharactors);
void left_pad(std::string& s, int padded_len, const char& pad_char);
void char_to_string(char c, std::string& s);
void replace_all(std::string& source, const std::string& from, const std::string& to);
//void readConfig(const std::string& fileName, json& config);
int string_byte_to_int(const std::string& s);
void format_time(const std::string& dateTimeT, std::string& formatedT, const std::string& decimalsT);
void upper_case_ascii(std::string& s);
void strip_non_alphanumeric(std::string& s);
unsigned char hexval(unsigned char c);
void hex2ascii(const std::string& in, std::string& out);
void string_to_hex(const std::string& input, std::string& output);
void erase_chars(std::string& s, const char c);
std::string where(const int line, const std::string& file, const std::string& function);

} // namespace strings

namespace times {

std::string get_gmt_time(const std::string& fmt);
void get_time(char* date, std::size_t size, const std::string& format);
void get_time(std::string& date, std::size_t size, const std::string& format);
void epoch_micro_time(std::string& microTime);
std::string get_epoch();
void get_epoch(std::string& epoch);
void get_epoch(std::string& epoch, const std::string& details);
void get_epoch(std::string& epoch, const std::string& details, std::string& dateTimeT, std::string& decimalsT);
void get_epoch_mili(std::string& epoch);
void get_epoch_micro(std::string& epoch);
void get_datetime_from_epoch(const std::string& epoch, std::string& dateTimeT);
std::string get_datetime_from_epoch(const int epoch, const std::string& format);
void get_datetime_from_epoch(const int epoch, std::string& dateTimeT, const std::string& format);
void get_datetime_from_epoch(const std::string& epoch, std::string& dateTimeT, const std::string& format);
std::string get_datetime_from_epoch(const std::string& epoch, const std::string& format);
void get_decimal_seconds_from_epoch(const std::string& epoch, std::string& decimalT, const std::string& details);
void get_datetime_from_epoch_with_decimal_seconds(const std::string& epoch, std::string& dateTimeT);

} // namespace times

namespace files {

bool fileExists(const std::string& name);

class IniParser {
public:
    void read(const std::string& fileName);
    void get(const std::string& section, const std::string& key, std::string& value);
    const std::string& get(const std::string& section, const std::string& key) const;
    std::string& get(const std::string& section, const std::string& key);

private:
    std::map<std::string, std::map<std::string, std::string>> config_;
    std::string value_{""};

};

} // namespace files

} // namespace mix

#endif // MIX_UTILITY_H
